package nl.davinci.pds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import nl.davinci.pds.GetLabelCodesRequest;
import nl.davinci.pds.GetLabelCodesResponse;
import nl.davinci.pds.ParamSet;
import nl.davinci.pds.RetrieveParamSetRequest;
import nl.davinci.pds.RetrieveParamSetResponse;
import nl.davinci.pds.RetrieveParamSetsInCategoryRequest;
import nl.davinci.pds.RetrieveParamSetsInCategoryResponse;
import nl.davinci.pds.RetrieveStringListParamRequest;
import nl.davinci.pds.RetrieveStringListParamResponse;
import nl.davinci.pds.common.transformer.PdsTransformer;
import nl.davinci.pds.domain.dao.db.PdsDao;
import nl.davinci.pds.domain.model.db.Label;
import nl.davinci.pds.domain.model.db.ParameterVersion;
import nl.davinci.pds.domain.model.db.StringListParam;

@Service
public class PdsServiceImpl implements PdsService {

	@Autowired
	private PdsDao dao;

	@Autowired
	private PdsTransformer transformer;

	@Transactional(readOnly = true)
	@Override
	public RetrieveParamSetResponse retrieveParamSet(RetrieveParamSetRequest request) {
		String parameterSetName = request.getName();
		String labelCode = request.getLabel();
		Date validAt = request.getDate();

		List<ParameterVersion> result = dao.findParameterVersionsBySet(parameterSetName, labelCode, validAt);
		RetrieveParamSetResponse response = transformer.transformParamSet(result);

		ParamSet paramSet = response.getParamSet();
		paramSet.setDate(validAt);
		paramSet.setLabel(labelCode);
		paramSet.setName(parameterSetName);

		return response;
	}

	@Transactional(readOnly = true)
	@Override
	public RetrieveStringListParamResponse retrieveStringListParam(RetrieveStringListParamRequest request) {
		List<String> parameterNames = request.getParameterNames();
		String labelCode = request.getLabel();
		Date validAt = request.getDate();

		List<StringListParam> dbParams = dao.findStringListParameters(parameterNames, labelCode, validAt);
		return transformer.transformStringListParams(dbParams);
	}

	@Transactional(readOnly = true)
	@Override
	public GetLabelCodesResponse getLabelCodes(GetLabelCodesRequest request) {
		List<Label> serverLabels = dao.findLabels(request.getDate());
		List<String> clientLabels = transformer.transformLabelCodes(serverLabels);

		GetLabelCodesResponse response = new GetLabelCodesResponse();
		response.setLabels(clientLabels);

		return response;
	}

	@Transactional(readOnly = true)
	@Override
	public RetrieveParamSetsInCategoryResponse retrieveParamSetsInCategory(RetrieveParamSetsInCategoryRequest request) {
		String categoryName = request.getCategoryName();
		String labelCode = request.getLabel();
		Date validAt = request.getDate();

		RetrieveParamSetsInCategoryResponse response = new RetrieveParamSetsInCategoryResponse();
		for (String parameterSetName : dao.parameterSetNamesInCategory(categoryName)) {
			List<ParameterVersion> result = dao.findParameterVersionsBySet(parameterSetName, labelCode, validAt);
			if (!result.isEmpty()) {
				ParamSet paramSet = transformer.transformParamSet(result).getParamSet();
				paramSet.setDate(validAt);
				paramSet.setLabel(labelCode);
				paramSet.setName(parameterSetName);
				response.getParamSets().add(paramSet);
			}
		}
		return response;
	}
}