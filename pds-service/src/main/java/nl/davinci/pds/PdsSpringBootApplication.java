package nl.davinci.pds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(value = "nl.davinci.pds.domain.dao")
@EntityScan(basePackages = "nl.davinci.pds.domain.model.db")
public class PdsSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdsSpringBootApplication.class, args);
	}
}
