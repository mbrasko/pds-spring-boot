package nl.davinci.pds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringResult;

import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBResult;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.util.Date;

import nl.davinci.pds.config.ConnectorConfig;
import nl.davinci.pds.config.WebServiceConfig;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigurationForTest.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
public class PdsSpringBootApplicationTests {

	private final static String LABEL = "Label-123";

	private final static String NAME = "SomeName";

	private WebServiceTemplate webServiceTemplate = new WebServiceTemplate();

	@LocalServerPort
	private int serverPort;

	@Autowired
	private Jaxb2Marshaller marshaller;

	@BeforeClass
	public static void disableBootstrap() {
		System.setProperty("spring.cloud.bootstrap.enabled", "false");
	}

	@Before
	public void setUp() {
		webServiceTemplate.setDefaultUri("http://localhost:" + serverPort + WebServiceConfig.CONTEXT_PATH);
	}

	@Test
	public void testSendingRequestAsXml() {
		final String request = "<par:RetrieveParamSetRequest xmlns:par=\"http://www.davincigroep.nl/2013/08/dhp/parameter-data-service\">\n" +
				"    <par:Name>" + NAME + "</par:Name>\n" +
				"    <par:Label>" + LABEL + "</par:Label>\n" +
				"    <par:Date>2016-07-31T00:00:00.000+02:00</par:Date>\n" +
				"</par:RetrieveParamSetRequest>";

		Source source = new StreamSource(new StringReader(request));
		Result result = new StringResult();
		webServiceTemplate.sendSourceAndReceiveToResult(source, result);

		String response = result.toString();
		assertThat(response).contains("<ns2:Name>" + NAME + "</ns2:Name>");
		assertThat(response).contains("<ns2:Label>" + LABEL + "</ns2:Label>");
		assertThat(response).contains("<ns2:Params/>");
	}

	@Test
	public void testSendingRequestAsJaxb() throws JAXBException {
		RetrieveParamSetRequest request = new RetrieveParamSetRequest();
		request.setName(NAME);
		request.setLabel(LABEL);
		request.setDate(new Date());

		Source source = new JAXBSource(marshaller.getJaxbContext(), request);
		JAXBResult result = new JAXBResult(marshaller.getJaxbContext());
		webServiceTemplate.sendSourceAndReceiveToResult(source, result);

		RetrieveParamSetResponse response = (RetrieveParamSetResponse) result.getResult();
		Assert.assertEquals(NAME, response.getParamSet().getName());
		Assert.assertEquals(LABEL, response.getParamSet().getLabel());
		Assert.assertTrue(LABEL, response.getParamSet().getParams().getBooleanParamsAndIntegerParamsAndDecimalParams().isEmpty());
	}

}

@Configuration
@EnableAutoConfiguration
@ComponentScan(
		excludeFilters = {@ComponentScan.Filter(
				type = FilterType.ASSIGNABLE_TYPE,
				classes = {ConnectorConfig.class, PdsSpringBootApplication.class}
		)}
)
class ConfigurationForTest {

}