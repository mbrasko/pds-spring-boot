package nl.davinci.pds.config;

import org.apache.catalina.Executor;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardThreadExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import java.util.Collection;

@PropertySource({"classpath:/tomcat-connector.properties"})
public abstract class ConnectorConfig {

	@Value("${MaxPostSize}")
	protected int maxPostSize;

	@Value("${AcceptCount}")
	protected String acceptCount;

	@Value("${MaxHttpHeaderSize}")
	protected int maxHttpHeaderSize;

	@Value("${MaxSwallowSize}")
	protected int maxSwallowSize;

	@Value("${AcceptorThreadCount}")
	protected int acceptorThreadCount;

	@Value("${AcceptorThreadPriority}")
	protected int acceptorThreadPriority;

	@Bean
	public EmbeddedServletContainerFactory tomcat() throws LifecycleException {
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		tomcat.setProtocol(getProtocol());
		tomcat.setContextLifecycleListeners(createListeners());
		tomcat.addConnectorCustomizers(createConnectorCustomizer());
		return tomcat;
	}

	protected abstract String getProtocol();

	protected abstract Collection<LifecycleListener> createListeners();

	private TomcatConnectorCustomizer createConnectorCustomizer() {
		return this::customizeConnector;
	}

	protected abstract void customizeConnector(Connector connector);

	protected Executor createExecutor() throws LifecycleException {
		StandardThreadExecutor executor = new StandardThreadExecutor();
		executor.setName("tomcatThreadPool");
		executor.setNamePrefix("catalina-exec-");
		executor.setMaxThreads(200);
		executor.setMinSpareThreads(25);
		executor.start();
		return executor;
	}
}
