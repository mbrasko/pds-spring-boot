package nl.davinci.pds.config;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.JreMemoryLeakPreventionListener;
import org.apache.catalina.core.ThreadLocalLeakPreventionListener;
import org.apache.catalina.mbeans.GlobalResourcesLifecycleListener;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//@Configuration
public class ConnectorWithHttp11NioProtocolConfig extends ConnectorConfig {

	@Value("${tomcat.connector.keystore}")
	private String keystoreFile;

	@Value("${tomcat.connector.keystore-password}")
	private String keystorePassword;

	@Value("${tomcat.connector.keyalias}")
	private String keyalias;

	@Override
	protected String getProtocol() {
		return "org.apache.coyote.http11.Http11NioProtocol";
	}

	@Override
	protected Collection<LifecycleListener> createListeners() {
		List<LifecycleListener> listeners = new ArrayList<>();
		listeners.add(new JreMemoryLeakPreventionListener());
		listeners.add(new GlobalResourcesLifecycleListener());
		listeners.add(new ThreadLocalLeakPreventionListener());
		return listeners;
	}

	@Override
	protected void customizeConnector(Connector connector) {
		connector.setScheme("https");
		connector.setSecure(true);
		connector.setPort(8443);
		connector.setURIEncoding("UTF-8");
		connector.setMaxPostSize(maxPostSize);
		connector.setProperty("acceptCount", acceptCount);

		Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
		protocol.setSSLEnabled(true);
		protocol.setKeystoreFile(keystoreFile);
		protocol.setKeystorePass(keystorePassword);
		protocol.setTruststoreFile(keystoreFile);
		protocol.setTruststorePass(keystorePassword);
		protocol.setKeyAlias(keyalias);
		protocol.setClientAuth("false");
		protocol.setCompression("on");
		protocol.setCompressionMinSize(1024);
		protocol.setCompressableMimeType("text/html,text/xml,text/plain,text/css,application/xhtml+xml,application/x-javascript,text/javascript");
		protocol.setServer("Tomcat");
		protocol.setMaxHttpHeaderSize(maxHttpHeaderSize);
		protocol.setMaxSwallowSize(maxSwallowSize);
		protocol.setAcceptorThreadCount(acceptorThreadCount);
		protocol.setAcceptorThreadPriority(acceptorThreadPriority);
		try {
			protocol.setExecutor(createExecutor());
		} catch (LifecycleException e) {
			throw new RuntimeException(e);
		}
	}
}
