package nl.davinci.pds.ws;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class CustomPdsHealthIndicator implements HealthIndicator {

	@Value("${translations.error-code:Error code}")
	private String errorCodeText;

	@Override
	public Health health() {
		int errorCode = check();
		if (errorCode != 0) {
			return Health.down().withDetail(errorCodeText, errorCode).build();
		}
		return Health.up().build();
	}

	private int check() {
		return Math.random() < 0.5 ? 0 : 1;
	}

}