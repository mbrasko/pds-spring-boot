package nl.davinci.pds.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import nl.davinci.pds.GetLabelCodesRequest;
import nl.davinci.pds.GetLabelCodesResponse;
import nl.davinci.pds.RetrieveParamSetRequest;
import nl.davinci.pds.RetrieveParamSetResponse;
import nl.davinci.pds.RetrieveParamSetsInCategoryRequest;
import nl.davinci.pds.RetrieveParamSetsInCategoryResponse;
import nl.davinci.pds.RetrieveStringListParamRequest;
import nl.davinci.pds.RetrieveStringListParamResponse;
import nl.davinci.pds.config.WebServiceConfig;
import nl.davinci.pds.service.PdsService;
import nl.davinci.security.auditlog.AuditLoggingFacade;

@Endpoint
public class PdsEndpoint {

	@Autowired
	private PdsService service;

	@Autowired
	private AuditLoggingFacade auditLoggingFacade;

	private static final Logger logger = LoggerFactory.getLogger(PdsEndpoint.class);

//	@RequiresRoles("RetrieveParamSet")
	@PayloadRoot(namespace = WebServiceConfig.NAMESPACE, localPart = "RetrieveParamSetRequest")
	@ResponsePayload
	public RetrieveParamSetResponse retrieveParamSet(@RequestPayload
															 RetrieveParamSetRequest request) {
		RetrieveParamSetResponse response = service.retrieveParamSet(request);
		log("User performed operation 'retrieveParamSet', with name: " + (request == null ? "" : request.getName()));
		return response;
	}

//	@RequiresRoles("RetrieveParamSetsInCategory")
	@PayloadRoot(namespace = WebServiceConfig.NAMESPACE, localPart = "RetrieveParamSetsInCategoryRequest")
	@ResponsePayload
	public RetrieveParamSetsInCategoryResponse retrieveParamSetsInCategory(@RequestPayload
																				   RetrieveParamSetsInCategoryRequest request) {
		RetrieveParamSetsInCategoryResponse response = service.retrieveParamSetsInCategory(request);
		log("User performed operation 'retrieveParamSetsInCategory', with name: " + (request == null ? "" : request.getCategoryName()));
		return response;
	}

//	@RequiresRoles("RetrieveStringListParam")
	@PayloadRoot(namespace = WebServiceConfig.NAMESPACE, localPart = "RetrieveStringListParamRequest")
	@ResponsePayload
	public RetrieveStringListParamResponse retrieveStringListParam(@RequestPayload
																		   RetrieveStringListParamRequest request) {
		RetrieveStringListParamResponse response = service.retrieveStringListParam(request);
		log("User performed operation 'retrieveStringListParam' and retrieved: " + response.getStringListParams().size() + " parameters");
		return response;
	}

//	@RequiresRoles("GetLabelCodes")
	@PayloadRoot(namespace = WebServiceConfig.NAMESPACE, localPart = "GetLabelCodesRequest")
	@ResponsePayload
	public GetLabelCodesResponse getLabelCodes(@RequestPayload
													   GetLabelCodesRequest request) {
		GetLabelCodesResponse response = service.getLabelCodes(request);
		log("User performed operation getLabelCodes");
		return response;
	}

	private void log(String message) {
		auditLoggingFacade.info(message);
		logger.debug(message);
	}
}
