URL: http://localhost:8080/ws/holiday

Sample request:
<soapenv:Envelope xmlns:sch="http://mycompany.com/hr/schemas" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
         <wsse:UsernameToken wsu:Id="UsernameToken-6FBA203A3837A361BD14706597337919">
            <wsse:Username>ja</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">EKa7XB8PA9xfZZhi+UHuRe3s5tU=</wsse:Password>
            <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">qPQeKO6dEozNZoYooSIqkQ==</wsse:Nonce>
            <wsu:Created>2016-08-08T12:35:33.790Z</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <sch:HolidayRequest>
         <!--You may enter the following 2 items in any order-->
         <sch:Holiday>
            <sch:StartDate>2016-08-08+02:00</sch:StartDate>
            <sch:EndDate>2016-08-24+02:00</sch:EndDate>
         </sch:Holiday>
         <sch:Employee>
            <sch:Number>1</sch:Number>
            <sch:FirstName>meno</sch:FirstName>
            <sch:LastName>priezvisko</sch:LastName>
         </sch:Employee>
      </sch:HolidayRequest>
   </soapenv:Body>
</soapenv:Envelope>

Result:
Search for e.g. "Booking holiday for [Mon Aug 08 00:00:00 CEST 2016 - Wed Aug 24 00:00:00 CEST 2016] for [meno priezvisko] " in system out