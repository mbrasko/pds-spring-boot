package nl.davinci.pds.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.server.EndpointExceptionResolver;

import javax.xml.namespace.QName;

import nl.davinci.soap.common.DavinciSoapFault;
import nl.davinci.soap.common.EnglishMessage;
import nl.davinci.soap.server.FaultDetailsCreator;
import nl.davinci.soap.server.FixedMessageEndpointExceptionResolver;
import nl.davinci.soap.server.RequestValidationErrorHandler;
import nl.davinci.soap.server.SoapFaultResponder;
import nl.davinci.soap.server.standard.DavinciEndpointExceptionResolver;
import nl.davinci.soap.server.standard.DavinciFaultDetailsCreator;
import nl.davinci.soap.server.standard.DavinciRequestValidationErrorHandler;
import nl.davinci.soap.server.standard.DavinciSoapFaultResponder;

@Configuration
public class PdsErrorHandlingConfig {

	@Bean
	public RequestValidationErrorHandler requestValidationErrorHandler(SoapFaultResponder<DavinciSoapFault> soapFaultResponder) {
		return new DavinciRequestValidationErrorHandler(soapFaultResponder, new EnglishMessage("Validation errors"), "Validation error");
	}

	private FaultDetailsCreator<DavinciSoapFault> faultDetailsCreator() {
		return new DavinciFaultDetailsCreator(new QName(WebServiceConfig.NAMESPACE, "FaultDetails"), new QName(WebServiceConfig.NAMESPACE, "Code"), new QName(
				WebServiceConfig.NAMESPACE, "Detail"));
	}

	@Bean
	public SoapFaultResponder<DavinciSoapFault> soapFaultResponder() {
		return new DavinciSoapFaultResponder(faultDetailsCreator());
	}

	@Bean
	public EndpointExceptionResolver endpointExceptionResolver() {
		DavinciEndpointExceptionResolver resolver = new DavinciEndpointExceptionResolver(soapFaultResponder());
		resolver.setOrder(0);
		return resolver;
	}

	@Bean
	public EndpointExceptionResolver fallbackEndpointExceptionResolver() {
		DavinciSoapFault fallbackFault = new DavinciSoapFault(new EnglishMessage("Unexpected server error"), "SERVER", "Server error");
		FixedMessageEndpointExceptionResolver<DavinciSoapFault> resolver = new FixedMessageEndpointExceptionResolver<>(soapFaultResponder(), fallbackFault);
		resolver.setOrder(100);
		return resolver;
	}
}
