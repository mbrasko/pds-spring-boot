package nl.davinci.pds.service;

import nl.davinci.pds.GetLabelCodesRequest;
import nl.davinci.pds.GetLabelCodesResponse;
import nl.davinci.pds.RetrieveParamSetRequest;
import nl.davinci.pds.RetrieveParamSetResponse;
import nl.davinci.pds.RetrieveParamSetsInCategoryRequest;
import nl.davinci.pds.RetrieveParamSetsInCategoryResponse;
import nl.davinci.pds.RetrieveStringListParamRequest;
import nl.davinci.pds.RetrieveStringListParamResponse;

public interface PdsService {

	RetrieveParamSetResponse retrieveParamSet(RetrieveParamSetRequest request);

	RetrieveParamSetsInCategoryResponse retrieveParamSetsInCategory(RetrieveParamSetsInCategoryRequest request);

	RetrieveStringListParamResponse retrieveStringListParam(RetrieveStringListParamRequest request);

	GetLabelCodesResponse getLabelCodes(GetLabelCodesRequest request);
}