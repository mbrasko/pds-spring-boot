package nl.davinci.pds.config;

import org.dozer.DozerBeanMapper;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;

import nl.davinci.pds.common.security.PdsAuditLoggingContext;
import nl.davinci.pds.common.transformer.PdsTransformer;
import nl.davinci.soap.server.RequestPayloadValidatingInterceptor;
import nl.davinci.soap.server.RequestValidationErrorHandler;
import nl.davinci.soap.server.ResponsePayloadValidatingInterceptor;
import nl.davinci.xml.UrlSource;

import static org.springframework.util.ResourceUtils.getURL;

@EnableWs
@Configuration
@Import({PdsAuditLoggingContext.class})
public class WebServiceConfig extends WsConfigurerAdapter {

	public static final String NAMESPACE = "http://www.davincigroep.nl/2013/08/dhp/parameter-data-service";

	private static final String PDS_SCHEMA = "schemas/pds-service.xsd";

	public static final String CONTEXT_PATH = "/pds-ws";

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, CONTEXT_PATH + "/*");
	}

	// This will generate wsdl schema accessible on URL:
	// https://localhost:8443/pds-ws/pds-service.wsdl
	@Bean(name = "pds-service")
	public DefaultWsdl11Definition defaultWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("ParameterDataHttpPort");
		wsdl11Definition.setLocationUri(CONTEXT_PATH);
		wsdl11Definition.setTargetNamespace(NAMESPACE);
		wsdl11Definition.setSchema(xsdSchema());
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema xsdSchema() {
		return new SimpleXsdSchema(new ClassPathResource(PDS_SCHEMA));
	}

	@Bean
	public PdsTransformer pdsTransformer() {
		return new PdsTransformer();
	}

	@Bean
	public DozerBeanMapper dozerMapper() {
		DozerBeanMapper dozerMapper = new DozerBeanMapper();
		dozerMapper.setMappingFiles(Collections.singletonList("nl/davinci/pds/common/transformer/dozer-mappings.xml"));
		return dozerMapper;
	}

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("nl.davinci.pds");
		return marshaller;
	}

	private Schema createSchema() throws FileNotFoundException, SAXException {
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Source source = new UrlSource(getURL("classpath:" + WebServiceConfig.PDS_SCHEMA));
		return schemaFactory.newSchema(new Source[]{source});
	}

	@Bean
	public PayloadRootAnnotationMethodEndpointMapping payloadRootAnnotationMethodEndpointMapping(
			RequestValidationErrorHandler requestValidationErrorHandler) throws IOException, SAXException {
		Schema schema = createSchema();
		PayloadRootAnnotationMethodEndpointMapping mapping = new PayloadRootAnnotationMethodEndpointMapping();
		mapping.setInterceptors(new EndpointInterceptor[]{
				requestPayloadValidatingInterceptor(schema, requestValidationErrorHandler),
				responsePayloadValidatingInterceptor(schema, requestValidationErrorHandler)
		});
		return mapping;
	}

	private RequestPayloadValidatingInterceptor requestPayloadValidatingInterceptor(
			Schema schema, RequestValidationErrorHandler requestValidationErrorHandler) throws IOException {
		return new RequestPayloadValidatingInterceptor(schema, requestValidationErrorHandler);
	}

	private ResponsePayloadValidatingInterceptor responsePayloadValidatingInterceptor(
			Schema schema, RequestValidationErrorHandler requestValidationErrorHandler) throws IOException {
		return new ResponsePayloadValidatingInterceptor(schema, requestValidationErrorHandler);
	}
}
