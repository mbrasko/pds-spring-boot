package nl.davinci.pds.config.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

@Component
public class SpringConfigClient {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@Value("${spring.cloud.config.username:username}")
	private String configUsername;

	@Value("${spring.cloud.config.password:password}")
	private String configPassword;

	@Value("${spring.cloud.config.uri:uri}")
	private String configUri;

	@Value("${spring.application.name:name}")
	private String applicationName;

	@Value("${config.custom.profile:profile}")
	private String configProfile;

	@Value("${config.custom.label:label}")
	private String configLabel;

	public String getResourceAsString(String resourcePath) {
		// e.g. http://localhost:8001/pds-service/local/master/logback.xml
		String resourceRemoteUri = configUri + "/" + applicationName + "/" + configProfile + "/" + configLabel + resourcePath;
		RestTemplate template = restTemplateBuilder.basicAuthorization(configUsername, configPassword).build();
		return template.getForObject(resourceRemoteUri, String.class);
	}

	public InputStream getResourceAsStream(String resourcePath) throws UnsupportedEncodingException {
		return new ByteArrayInputStream(getResourceAsString(resourcePath).getBytes("UTF-8"));
	}

}
