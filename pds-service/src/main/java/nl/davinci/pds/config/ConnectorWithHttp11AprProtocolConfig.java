package nl.davinci.pds.config;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.core.JreMemoryLeakPreventionListener;
import org.apache.catalina.core.ThreadLocalLeakPreventionListener;
import org.apache.catalina.mbeans.GlobalResourcesLifecycleListener;
import org.apache.coyote.http11.Http11AprProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
public class ConnectorWithHttp11AprProtocolConfig extends ConnectorConfig {

	@Value("${tomcat.connector.SSLCipherSuite}")
	private String sslCipherSuite;

	@Value("${tomcat.connector.SSLCertificateChainFile}")
	private String sslCertificateChainFile;

	@Value("${tomcat.connector.SSLCertificateFile}")
	private String sslCertificateFile;

	@Value("${tomcat.connector.SSLCertificateKeyFile}")
	private String sslCertificateKeyFile;

	@Override
	protected String getProtocol() {
		return "org.apache.coyote.http11.Http11AprProtocol";
	}

	@Override
	protected Collection<LifecycleListener> createListeners() {
		List<LifecycleListener> listeners = new ArrayList<>();
		listeners.add(new JreMemoryLeakPreventionListener());
		listeners.add(new GlobalResourcesLifecycleListener());
		listeners.add(new ThreadLocalLeakPreventionListener());
		listeners.add(new AprLifecycleListener());
		return listeners;
	}

	@Override
	protected void customizeConnector(Connector connector) {
		connector.setScheme("https");
		connector.setSecure(true);
		connector.setPort(8443);
		connector.setURIEncoding("UTF-8");
		connector.setMaxPostSize(maxPostSize);
		connector.setProperty("acceptCount", acceptCount);

		Http11AprProtocol protocol = (Http11AprProtocol) connector.getProtocolHandler();
		protocol.setCompression("on");
		protocol.setCompressionMinSize(1024);
		protocol.setCompressableMimeType("text/html,text/xml,text/plain,text/css,application/xhtml+xml,application/x-javascript,text/javascript");
		protocol.setServer("Tomcat");
		protocol.setMaxHttpHeaderSize(maxHttpHeaderSize);
		protocol.setMaxSwallowSize(maxSwallowSize);
		protocol.setAcceptorThreadCount(acceptorThreadCount);
		protocol.setAcceptorThreadPriority(acceptorThreadPriority);
		try {
			protocol.setExecutor(createExecutor());
		} catch (LifecycleException e) {
			throw new RuntimeException(e);
		}

		protocol.setSSLEnabled(true);
		protocol.setSSLProtocol("TLSv1+TLSv1.1+TLSv1.2");
		protocol.setSSLVerifyClient("none");
		protocol.setSSLDisableCompression(true);
		protocol.setSSLHonorCipherOrder("true");
		protocol.setSSLCipherSuite(sslCipherSuite);
		protocol.setSSLCertificateChainFile(sslCertificateChainFile);
		protocol.setSSLCertificateFile(sslCertificateFile);
		protocol.setSSLCertificateKeyFile(sslCertificateKeyFile);
	}
}
